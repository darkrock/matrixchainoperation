package matrixChainMultiplication;

/**
 * Created by doug on 2/2/2015.
 * Arc object
 */
import java.util.Arrays;
import java.util.ArrayList;

public class Arc {
    private Vertex v1;
    private Vertex v2;
    private Arc ceiling;
    private ArrayList<Arc> sons =  new ArrayList<Arc>();
    private int numeratorOfWeight;
    private int denominatorOfWeight;
    private double supportingWeight = 0.0;

    public Arc(Vertex v1, Vertex v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public Vertex getV1() { return v1; }

    public Vertex getV2() {
        return v2;
    }

    public int getMinWeight() {
        return Math.min(v1.getWeight(), v2.getWeight());
    }

    public Vertex getMinVertex() {
        if (v1.getWeight() <= v2.getWeight()) return v1;
        return v2;
    }

    public Arc getCeiling() {
        return ceiling;
    }

    public void setCeiling(Arc ceiling) {
        this.ceiling = ceiling;
    }

    public ArrayList getSons() {
        return sons;
    }

    public void addSons(ArrayList kids) {
        for(Object kid : kids)
            if (!this.sons.contains(kid))
                this.sons.add((Arc) kid);
    }
    /*// overload
    public void addSons(Arc kid) {
        if (sons == null)
            sons.add(kid);

        if (!this.sons.contains(kid))
            this.sons.add(kid);
    }

    public void addSons(Arc[] son) {
        for(Arc addthis : son){
            if this.sons.
        }
        this.sons = concat(son, this.sons);
    }*/

    public int getNumeratorOfWeight() {
        return numeratorOfWeight;
    }

    public void setNumeratorOfWeight(int numeratorOfWeight) {
        this.numeratorOfWeight = numeratorOfWeight;
    }

    public int getDenominatorOfWeight() {
        return denominatorOfWeight;
    }

    public void setDenominatorOfWeight(int denominatorOfWeight) {
        if (denominatorOfWeight != 0) this.supportingWeight = numeratorOfWeight * 1.0 / denominatorOfWeight;
        this.denominatorOfWeight = denominatorOfWeight;
    }

    public double getSupportingWeight() {
        if (this.denominatorOfWeight == 0) return 0;
        this.supportingWeight = numeratorOfWeight * 1.0 / this.denominatorOfWeight;
        return supportingWeight;
    }


    @Override
    public String toString() {
        return "Arc{" +
                "v1=" + v1.toString() +
                ", v2=" + v2.toString() +
                "}\n";
    }

    // @Override
    public String toStringMin() {
        return "Arc{" +
                "v1=" + v1.toString() +
                ", v2=" + v2.toString() +
                "\nceiling=" + ceiling +
                "\nsons=" + sons +
                "\nnumeratorOfWeight=" + numeratorOfWeight +
                "\ndenominatorOfWeight=" + denominatorOfWeight +
                "\nsupportingWeight=" + supportingWeight +
                '}';
    }

    private static Arc[] concat(Arc[] a, Arc[] b) {
        // System.out.println("checking nulls in Arc[] concat()");
        if (a == null) {
            System.out.println("null a, return b = " + b);
            return b;
        }
        if (b == null) {
            System.out.println("null b, return a = " + a);
            return a;
        }
        System.out.println("no nulls found");

        int aLen = a.length;
        int bLen = b.length;
        Arc[] c= new Arc[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        // System.out.println("\tArc[] list from concat():");
        for (Arc arc : c){
            System.out.print( arc);
        }
        // System.out.println("\t:Arc[] list from concat()");
        return c;
    }

}
