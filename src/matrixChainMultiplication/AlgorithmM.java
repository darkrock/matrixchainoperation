package matrixChainMultiplication;

import java.util.ArrayList;

/**
 * Implemented by doug on 2/16/2015.
 *
 * (I) Get all the potential h-arcs of the polygon by the one-sweep algorithm.
 * (All these arcs form a vertical list, with the highest arc closest to the maximum vertex
 * Vn and the lowest arc closest to the minimum vertex V1.)
 *
 * (II) Process the potential h-arcs one by one, from the top to the bottom. Let hi be the
 * potential h-arc being processed, let hk be the potential h-arc immediately above hi,
 * and let hi be the potential h-arc immediately below hi in the monotone polygon.
 * (If hi is the highest potential h-arc in the polygon, hk will be the degenerate arc hn;
 * if hi is the lowest potential h-arc in the polygon, hi will be the degenerated arc hi.)
 * Note that by the time we start processing hi, we have already obtained the l-optimum
 * partition of the subpolygon between hi and h,. We have also located the ceilings of
 * every h-arc in the l-optimum partition of this subpolygon. When we process hi, we
 * first locate the ceiling of hi and condense all hi’s descendants into hi. Then we obtain
 * the/-optimum partition of the subpolygon between hi and h, by deleting those blocks
 * of arcs which cannot exist in the/-optimum partition of the subpolygon between hi
 * and hn.
 */
public class AlgorithmM {

    static public ArrayList algorithmM(ArrayList OrderedList, int[] p) {
        ArrayList OptList;
        OptList = OrderedList;
        Vertex[] oPoly = OneSweep.getVertexes(p);
        int numerator;
        int denominator;
        String loopLocation = "init: ";
        int debug = 0, debugPart1 = 0;

        // create reusable sum of adjacent products array
        // sum of adjacent products Wi:Wj is by CP[j]-CP[i]
        int[] CP = new int[p.length];

        // CP[0] = 0;
        CP[0] = 0;
        for (int i = 1; i < CP.length; i++) {
            CP[i] = CP[i-1] + oPoly[i-1].getWeight() * oPoly[i].getWeight();

            // System.out.println("CP[" + i + "] = " + CP[i] + "\t" + oPoly[i - 2].getWeight() + "\t" + oPoly[i - 1].getWeight()); // debug
        }

        // set current to Arc at top of list
        Arc hJcurrent = (Arc) OrderedList.get(0);
        Arc hIbelow = (Arc) OrderedList.get(1);

        // create top degenerate arc at top of list.
        // not counted, all local maximum vertexes already captured
        // may need to eliminate this step
        // good god, man, I have to get this finished!
        int vertexMax = hJcurrent.getV1().getOrder() + 1;
        // the second vertex is 'fake', with an inconsequential weight of 1
        Arc hNtop = new Arc(oPoly[vertexMax], new Vertex(oPoly[vertexMax].getOrder(), 1));
        hNtop.setCeiling(hNtop);
        // toDO there has to be a better way to add a single item here. >:(
        // this is soooo dumb. I wish i was smarter.
        ArrayList<Arc> single = new ArrayList<Arc>();
        single.add(hNtop);
        hNtop.addSons(single);
        // add the Arc to the list
        // OrderedList.add(0, hNtop);
        Arc hKabove = hNtop;


        // create bottom degenerate arc at bottom of list.
        // the second vertex is 'fake', with an inconsequential weight of 1
        Arc h1bottom = new Arc(oPoly[0], new Vertex(oPoly[0].getOrder(), 1));
        OrderedList.add(h1bottom);

        // System.out.println("Ordered List\n" + OrderedList + "\nOrdered list complete");
        while (hJcurrent != h1bottom) {
            loopLocation = "Algorithm M loop : ";
            System.out.println(loopLocation + debug++);
            if (debug > 5) {
                System.exit(0);
            }

            // (part 1 of Algorithm M)
            // locates ceiling of current arc being processed:
            while (hJcurrent.getSupportingWeight() <= hKabove.getSupportingWeight()) {
                hJcurrent.setCeiling(hKabove);
                loopLocation = "part 1 while loop: ";
                // System.out.println(loopLocation + debugPart1++);
                if (debugPart1 > 7) {
                    System.exit(0);
                }

                // combine hKabove and all its descendants into hJcurrent and calculate S(hJcurrent\hKabove's ceiling)
                hJcurrent.addSons(hKabove.getSons());

                int leftSumOfWeights = CP[hKabove.getV1().getOrder()] - CP[hJcurrent.getV1().getOrder()];
                int arcAboveProductOfWeights = hKabove.getV1().getWeight() * hKabove.getV2().getWeight();
                int arcAboveSumOfWeights = CP[hKabove.getV2().getOrder()] - CP[hKabove.getV1().getOrder()];
                int rightSumOfWeights = CP[hJcurrent.getV2().getOrder()] - CP[hKabove.getV2().getOrder()];
                int currentProductOfWeights = hJcurrent.getV1().getWeight() * hJcurrent.getV2().getWeight();
                int currentSumOfWeights = CP[hJcurrent.getV2().getOrder()] - CP[hJcurrent.getV1().getOrder()];
                int productFirst2 = CP[(hJcurrent.getV1().getOrder() + 1)] - CP[hJcurrent.getV1().getOrder()];
                int productLast2 = CP[hJcurrent.getV2().getOrder()] - CP[(hJcurrent.getV2().getOrder() - 1)];
                int mergedAndMin = 0;
                boolean degenArc = false;
                int sumToRemoveForMinVertex = 0;
                // ternary example: minVal = a < b ? a : b;
                int ceilingNumerator = (hJcurrent.getCeiling() == null) ? 0 : hJcurrent.getCeiling().getNumeratorOfWeight();

                // calculate S(hJcurrent\hKabove's ceiling)
                // numerator is fan between hJcurrent and hKabove plus hKabove.getNumeratorOfWeight

                if ((hJcurrent.getV2().getOrder() - hJcurrent.getV1().getOrder()) < 2) {
                    System.err.println("arc " + hJcurrent + "is along edge!!!!");
                }
                if (hJcurrent.getMinVertex() == hJcurrent.getV1()) {
                    // min vertex is on left
                    // remove sum that includes min vertex
                    sumToRemoveForMinVertex = productFirst2;
                    // check for values merged
                    if (leftSumOfWeights == 0) {
                        mergedAndMin = arcAboveProductOfWeights;
                    }
                    System.out.print("\nMinV: L ");
                }
                if (hJcurrent.getMinVertex() == hJcurrent.getV2()) {
                    // min vertex is on right
                    // remove sum that includes min vertex
                    sumToRemoveForMinVertex = productLast2;
                    // check for values merged
                    if (rightSumOfWeights == 0) {
                        mergedAndMin = arcAboveProductOfWeights;
                    }
                    System.out.print("\nMinV R ");
                }

                hJcurrent.setNumeratorOfWeight((leftSumOfWeights + arcAboveProductOfWeights - mergedAndMin + rightSumOfWeights - sumToRemoveForMinVertex) *
                        hJcurrent.getMinWeight() ); // + ceilingNumerator??


                // check for degenerate arc
                if ((hJcurrent.getV2().getOrder() - hJcurrent.getV1().getOrder()) == 2) {
                    // this condition guarantees degenerate arc above.
                    degenArc = true;
                    hJcurrent.setNumeratorOfWeight(currentProductOfWeights * hKabove.getV1().getWeight());
                }

                System.out.println("Numerator = " + hJcurrent.getNumeratorOfWeight());


                // this should be correct for all cases:

                if (degenArc) {
                    hJcurrent.setDenominatorOfWeight(
                            (CP[hJcurrent.getV2().getOrder()] - CP[hJcurrent.getV1().getOrder()]) - currentProductOfWeights);
                } else {
                    hJcurrent.setDenominatorOfWeight(
                            (currentSumOfWeights - (arcAboveSumOfWeights - arcAboveProductOfWeights)) - currentProductOfWeights);
                }
                System.out.println("Denominator = " + hJcurrent.getDenominatorOfWeight());


                // Replace hKabove by hKabove's ceiling, ie, hKabove is always used to denote the lowest h-arc
                // above hJcurrent which is not yet combined into hJcurrent.
                hKabove = hKabove.getCeiling();
                hJcurrent.setCeiling(hKabove);
                loopLocation = "part 1 end while loop: ";
            }

            System.out.println("Ceiling of: " + hJcurrent);
            System.out.println("Set to: " + hJcurrent.getCeiling());

            // (part 2 of Algorithm M)
            while (hJcurrent.getSupportingWeight() >= hIbelow.getMinWeight()) {
                loopLocation = "part 2 while loop: ";
                // System.out.println(loopLocation);
                //      a. Delete hJcurrent and all its descendants from the list of potential h-arcs.
                // To delete those blocks of arcs which cannot exist in the l-optimum partition
                // of the subpolygon between hIbelow and hNtop; while (hJcurrent.getSupportingWeight() >= hIbelow.getMinWeight()) {
                //      a. Delete hJcurrent and all its descendants from the list of potential h-arcs.
                OrderedList.remove(hJcurrent);

                /* debug testing
                // ToDone : not sure this loop will actually work as expected
                Arc [] testing =  {hJcurrent, hIbelow, h1bottom};
                hJcurrent.addSons(testing);
                /* debug testing */

                if (hJcurrent.getSons() != null) {
                    for (Object son : hJcurrent.getSons()) {
                        OrderedList.remove(son);
                    }
                }
                System.out.println("after removal:" + OptList);


                //      b. Replace hJcurrent by the ceiling of hJcurrent;
                //          i.e., hJcurrent is always used to denote the arc immediately above
                //          hIbelow in the subpolygon between hIbelow and hNtop.
                hJcurrent = hJcurrent.getCeiling();
                System.out.println("After removal current arc = " + hJcurrent);
                System.out.println("current arc ceiling supporting weight = " +
                        hJcurrent.getSupportingWeight());
                System.out.println("hIbelow.getMinWeight = " + hIbelow.getMinWeight());
                loopLocation = "bottom of part2: ";
            }
            // (part 3 of Algorithm M)
            loopLocation = "part 3 setup: ";
            // Prepare to process next arc
            // replace hKabove by hJcurrent
            hKabove = hJcurrent;
            // replace hJcurrent by hIbelow
            hJcurrent = hIbelow;
            // replace hIbelow by the next arc in the list.
            if (hIbelow != h1bottom) {
                hIbelow = (Arc) OrderedList.get(OrderedList.indexOf(hIbelow) + 1);
            }
            loopLocation = "part 3 end: ";

        }
        // remove degenerate arc from Ordered list:
        OrderedList.remove(h1bottom);
        return OrderedList;
    }
}
/*




        // while (hJcurrent != h1bottom)
        for (int i = 1; i < OrderedList.size() - 1; i++) {
            // (part 1 of Algorithm M)
            // To locate the ceiling of hJcurrent.
            // if (true /* hKabove != hNtop ) {
                // non - initialized loop
                // While S(hJcurrent\hKabove) <= S(hKabove\hKabove’s ceiling)
                // (currently, hKabove is a son of hJcurrent)
                // hJcurrent.addSons(new Arc[]{hKabove});
                while (hJcurrent.getSupportingWeight() <= hKabove.getSupportingWeight()) {
                    debug ++;
                    if (debug > 8) System.out.println("loop > 10 : ");
                    System.out.println("entered -::while (hJcurrent.getSupportingWeight() (" + hJcurrent.getSupportingWeight() +
                            ") <= (" + hKabove.getSupportingWeight() + ") hKabove.getSupportingWeight())");

                    // combine hKabove and all its descendants into hJcurrent and calculate S(hJcurrent\hKabove's ceiling)
                    hJcurrent.addSons(hKabove.getSons());

                    numerator = hJcurrent.getMinWeight() * (
                            (CP[hKabove.getV1().getOrder()] - CP[hJcurrent.getV1().getOrder()]) +
                            (CP[hJcurrent.getV2().getOrder()] - CP[hKabove.getV2().getOrder()])
                    );
                    System.out.println(numerator);
                    denominator = (
                                    (CP[hKabove.getV1().getOrder()] - CP[hJcurrent.getV1().getOrder()]) +
                                    (hKabove.getV1().getWeight() * hKabove.getV2().getWeight()) +
                                    (CP[hJcurrent.getV2().getOrder()] - CP[hKabove.getV2().getOrder()]) -
                                    (hJcurrent.getV1().getWeight() * hJcurrent.getV2().getWeight())
                    );
                    hJcurrent.setNumeratorOfWeight(numerator);
                    hJcurrent.setDenominatorOfWeight(denominator);
                    // hJcurrent.setSupportingWeight((1.0 * numerator) / denominator);

                    // Replace hKabove by hKabove's ceiling, ie, hKabove is always used to denote the lowest h-arc
                    // above hJcurrent which is not yet combined into hJcurrent.
                    hKabove = hKabove.getCeiling();
                    System.out.println(hJcurrent + " = current arc: numerator = " + numerator + ", denominator = " + denominator
                                    + ", supporting weight = " + hJcurrent.getSupportingWeight() );
                }
            } else {
                // procedure for initial loop
                numerator = hJcurrent.getV1().getWeight() * (CP[hJcurrent.getV2().getOrder()] - CP[hJcurrent.getV1().getOrder()]);
                denominator = (
                        (CP[hKabove.getV1().getOrder()] - CP[hJcurrent.getV1().getOrder()])  +
                                // remember, the term below, hKabove.getV2().getWeight() == 1,
                                // since in this case it is a degenerate arc
                        (hKabove.getV1().getWeight() * hKabove.getV2().getWeight()) +
                        (CP[hJcurrent.getV2().getOrder()] - CP[hKabove.getV2().getOrder()]) -
                        (hJcurrent.getV1().getWeight() * hJcurrent.getV2().getWeight())
                );
                hJcurrent.setCeiling(hKabove);
                hJcurrent.addSons(new Arc[] {hKabove});
                hJcurrent.setNumeratorOfWeight(numerator);
                hJcurrent.setDenominatorOfWeight(denominator);
                hJcurrent.setSupportingWeight( ((double) numerator * 1.0) / denominator);

                System.out.println("1 time init: num = " + numerator + ", denom = " + denominator);
            }

            // (part 2 of Algorithm M)
            // To delete those blocks of arcs which cannot exist in the l-optimum partition
            // of the subpolygon between hIbelow and hNtop;

            // While C(hIbelow, hJcurrent and hJcurrent’s descendants, hJcurrent’s ceiling) >= Ho(hIbelow, hJcurrent’s ceiling);
            //      i.e., S(hJcurrent\hJcurrent’s ceiling) >= min (wi, wi'). Do

            while (hJcurrent.getSupportingWeight() >= hIbelow.getMinWeight()) {
                //      a. Delete hJcurrent and all its descendants from the list of potential h-arcs.
                System.out.println("current arc\\current arc ceiling supporting weight = " + hJcurrent.getSupportingWeight());
                System.out.println("hIbelow.getMinWeight = " + hIbelow.getMinWeight());
                System.out.println("supporting weight indicates arc and Sons cannot exist.\nList prior to removal:\n" + OptList);
                OrderedList.remove(hJcurrent);
                if (hJcurrent.getSons() != null) {
                    for (Arc son : hJcurrent.getSons()) {
                        OrderedList.remove(son);
                    }
                }
                System.out.println("after removal:" + OptList);

                //      b. Replace hJcurrent by the ceiling of hJcurrent;
                //          i.e., hJcurrent is always used to denote the arc immediately above
                //          hIbelow in the subpolygon between hIbelow and hNtop.
                hJcurrent = hJcurrent.getCeiling();
            }

            // Prepare to process next arc
            // (part 3 of Algorithm M)
            // replace hKabove by hJcurrent
            hKabove = hJcurrent;
            // replace hJcurrent by hIbelow
            hJcurrent = hIbelow;
            // replace hIbelow by the next arc in the list.
            hIbelow = (Arc) OrderedList.get(0);

            System.out.println("next arc 'hIbelow' = " + hIbelow);
        }

        return OrderedList;
    }

}
*/
