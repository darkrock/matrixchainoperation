package matrixChainMultiplication;

/**
 * Created by doug on 2/2/2015.
 * Vertex object
 */
public class Vertex {
    private int order;
    private int weight;

    public Vertex(int order, int weight) {
        this.order = order;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "order=" + order +
                ", weight=" + weight +
                '}';
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
