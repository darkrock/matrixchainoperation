package matrixChainMultiplication;

import java.util.*;

/**
 * Implemented by doug on 2/1/2015.
 * Generates list of potential horizontal arcs
 */
public class OneSweep {

    static public ArrayList oneSweep(int[] p){
        // this portion of algorithm runs in O(n) time. (verify)

        // local storage declaration
        Stack st = new Stack();
        ArrayList potentialHorizontalArcs = new ArrayList();

        Vertex[] oPoly = getVertexes(p);
        // System.out.println("start step a");
        Boolean pushed;
        for ( Vertex item : oPoly){  // http://stackoverflow.com/questions/85190/how-does-the-java-for-each-loop-work
            pushed = false;
            while (!pushed) {
                if (st.size() <= 1 || ((Vertex) st.peek()).getWeight() <= item.getWeight()) {
                    st.push(item);
                    pushed = true;
                    // System.out.println(st.peek() + " vertex Vc pushed");  // debug
                } else if (st.size() >= 2 && ((Vertex) st.peek()).getWeight() > item.getWeight()) {
                    st.pop(); // pop the top
                    Arc temp = new Arc((Vertex) st.peek(), item); // new Arc Vt-1 - Vc to potential arcs
                    // st.push(item);
                    potentialHorizontalArcs.add(0, temp);
                    // System.out.println("arc" + temp + " added"); // debug
                } else {
                    System.out.println("Should never get here in part A of oneSweep");
                }
            }
        }
        // System.out.println("step a complete");
        // step b of oneSweep

        /*// debug
        if (st.size() <= 3) {
            System.out.println("step b skipped, stack less than or equal to 3");
        }
        //*/
        while (st.size() > 3) {
            // System.out.println("popping " + st.peek() + " (Vt)");
            st.pop(); // pop the top
            Arc temp = new Arc((Vertex) st.peek(), (Vertex) st.get(0));
            potentialHorizontalArcs.add(temp);
            // System.out.println("arc" + temp + " added (Vt-1 - V1");
        }

        //reverse listing
        Collections.reverse(potentialHorizontalArcs);

        // System.out.println("step b complete");
        return potentialHorizontalArcs;
    }

    public static Vertex[] getVertexes(int[] p) {
        int idx = 0;
        /*
        identify index of first smallest dimension in p
        Since this can be done when reading matrix values to start, no need to count this step.
        */
        for (int i = 1; i < p.length; i++) {
            if(p[i] < p[idx]){
                idx = i;
            }
        }
        // create new Vertex set starting at idx
        Vertex [] oPoly = new Vertex [p.length];
        Vertex test = new Vertex(1,2);
        for (int i = 0; i < p.length; i++){
            oPoly[i] = new Vertex(i, p[idx + i]);
            //p[idx + i];
            if (idx + i >= p.length - 1)
                idx = idx - p.length;
        }
        /*  debug for new array
        System.out.println("compare:");
        System.out.println( Arrays.toString(p) );
        System.out.println( Arrays.toString(pOrder) );
        //*/
        return oPoly;
    }
}
