package matrixChainMultiplication;

/**
 * O(n²) code from wikipedia: http://en.wikipedia.org/wiki/Matrix_chain_multiplication
 */

public class MatrixOrderOptimization {
    protected int[][]m;
    protected int[][]s;
    protected int[] matrix;

    public void matrixChainOrder(int[] p) {
        matrix = p;

        int n = p.length - 1;
        m = new int[n][n];
        s = new int[n][n];
        for (int i = 0; i < n; i++) {
            m[i] = new int[n];
            m[i][i] = 0;
            s[i] = new int[n];
        }
        for (int ii = 1; ii < n; ii++) {
            for (int i = 0; i < n - ii; i++) {
                int j = i + ii;
                m[i][j] = Integer.MAX_VALUE;
                for (int k = i; k < j; k++) {
                    int q = m[i][k] + m[k+1][j] + p[i]*p[k+1]*p[j+1];
                    if (q < m[i][j]) {
                        m[i][j] = q;
                        s[i][j] = k;
                    }
                }
            }
        }
    }

    public void printOptimalParenthesizations() {
        boolean[] inAResult = new boolean[s.length];
        printOptimalParenthesizations(s, 0, s.length - 1, inAResult);
    }

    void printOptimalParenthesizations(int[][]s, int i, int j,  /* for pretty printing: */ boolean[] inAResult) {
        if (i != j) {
            printOptimalParenthesizations(s, i, s[i][j], inAResult);
            printOptimalParenthesizations(s, s[i][j] + 1, j, inAResult);
            String istr = inAResult[i] ? "_result " : " ";
            String jstr = inAResult[j] ? "_result " : " ";
            System.out.println(" A_" + i + istr + "* A_" + j + jstr);
            inAResult[i] = true;
            inAResult[j] = true;
        }
    }

    public void printWork() {
        System.out.println("\nwork product:\n");
        // print m matrix
        outputMatrix(m);
        //print s matrix
        outputMatrix(s);
    }

    private void outputMatrix(int[][] mToPrint){
        //* print header
        System.out.printf("%8s", " ");
        for (int i = 0; i < matrix.length - 1; i++) {
            System.out.format("%8s",i + " " + matrix[i] + "x" + matrix[i + 1]);
        }
        System.out.println();

        // print result matrix body
        for (int row = 0; row < mToPrint.length; row++) {
            // print matrix dimensions on left hand side
            System.out.printf("%8s", matrix[row] + " by " + matrix[row + 1]);
            for (int col = 0; col < mToPrint[row].length; col++) {
                if (row == col || mToPrint[row][col] > 0){
                    System.out.printf("%8d", mToPrint[row][col]);
                } else {
                    System.out.printf("%8s", "*");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}