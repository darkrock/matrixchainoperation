package matrixChainMultiplication;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Vertex [] oPoly;

        System.out.println("Matrix Chain Optimization comparison");

        // int [] matrixChain = {11,34,14,40,16,20,23,37,28,27,12,18};
        // int [] matrixChain1 = {44,14,40,16,2,23,37,28,43,12};
        // int [] matrixChain0 = {25,40,12,10,11}; // part I, pg 369, example 5
        // int [] matrixChain0 = {14,4,12,9,17,6,15};
        int [] matrixChain0 = {10,11,16,35,15,11}; // part II, pg 235, example 6a
        // int [] matrixChain0 = {11,34,14,40,16,20,23,37,28,27,12,18}; // pg 241, selection
        // int [] matrixChain0 = {10,12,17,20,13}; // part II, pg 235, example 6b
        /*
        MatrixOrderOptimization solve = new MatrixOrderOptimization();
        System.out.println("\nChain 0");
        solve.matrixChainOrder(matrixChain0);
        solve.printOptimalParenthesizations();
        System.out.println("^^ O(n^2) algorithm");
        */
        // solve.matrixChainOrder(matrixChain1);
        // solve.printWork();

        ArrayList sweepList = OneSweep.oneSweep(matrixChain0);
        // System.out.println("SweepList result\n" + sweepList);
        System.out.println("sweeplist complete \n");

        ArrayList Mlist =  AlgorithmM.algorithmM(sweepList, matrixChain0);
        System.out.println("M Algorithm result\n" + Mlist);

        System.out.println("\nGoodbye for now");
    }
}
